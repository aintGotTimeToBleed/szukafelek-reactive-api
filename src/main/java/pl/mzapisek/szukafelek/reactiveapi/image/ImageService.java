package pl.mzapisek.szukafelek.reactiveapi.image;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ImageService {

    private final ImageRepository imageRepository;

    public ImageService(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    public Flux<Image> findImages() {
        return imageRepository.findAll();
    }

    public Mono<Image> findImage(Integer id) {
        return imageRepository.findById(id);
    }

    public Mono<Image> saveImage(Image image) {
        // todo if mainimage set other images to non main first
        return imageRepository.save(image);
    }

    public Flux<Image> findImagesByTileId(Integer tileId) {
        return imageRepository.findImagesByTileId(tileId);
    }

    public Mono<Image> findMainImageByTileId(Integer tileId) {
        return imageRepository.findMainImageByTileId(tileId);
    }

}
