package pl.mzapisek.szukafelek.reactiveapi.image;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.http.codec.multipart.FormFieldPart;
import org.springframework.http.codec.multipart.Part;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/images")
public class ImageController {

    private String hostname;

    private final StorageService storageService;
    private final ImageService imageService;
    private final ImageRepository imageRepository;

    public ImageController(StorageService storageService,
                           ImageService imageService,
                           ImageRepository imageRepository,
                           @Value("${hostname}") String hostname) {
        this.storageService = storageService;
        this.imageService = imageService;
        this.imageRepository = imageRepository;
        this.hostname = hostname;
    }

    @GetMapping
    public Flux<EntityModel<Image>> getImages() {
        return imageService.findImages().map(this::createEntityModel);
    }

    // todo search by colour, pattern, shape, size
    @GetMapping("{id}")
    public Mono<Image> getImage(@PathVariable Integer id) {
        return imageRepository.findById(id);
    }

    // todo @RequestPart("file") FilePart file instead of ServerWebExchange??
    @PostMapping("{id}")
    public Mono<Image> upload(@PathVariable Integer tileId, ServerWebExchange exchange) {
        return exchange.getMultipartData()
                .flatMap(parts -> {
                    // todo refactor move logic to service
                    Map<String, Part> stringPartMap = parts.toSingleValueMap();
                    String fileName = UUID.randomUUID().toString();
                    FilePart file = (FilePart) stringPartMap.get("file");

                    storageService.store(file, fileName); // move to then? its not subscribed

                    FormFieldPart name = (FormFieldPart) stringPartMap.get("name");
                    FormFieldPart setAsMainImage = (FormFieldPart) stringPartMap.get("setAsMainImage");

                    return imageService.saveImage(new Image(null, tileId, fileName, name.value(), Boolean.valueOf(setAsMainImage.value())));
                });
    }

    @GetMapping(value = "{id}/download", produces = MediaType.IMAGE_JPEG_VALUE)
    public Mono<ResponseEntity<Resource>> download(@PathVariable Integer id) {
        // todo move this to service?
        // todo what types other types like png? -> https://www.baeldung.com/java-file-mime-type
        return imageRepository.findById(id)
                .flatMap(image -> storageService.load(image.getUuidFileName()))
                .map(resource -> ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + resource.getFilename() + ".jpg")
                        .body(resource))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    // todo images/uuidName/download
    private EntityModel<Image> createEntityModel(Image image) {
        return EntityModel.of(image,
                Link.of(hostname + "/images/" + image.getId()).withSelfRel(),
                Link.of(hostname + "/images/" + image.getId() + "/download").withRel("download image"));
    }

}
