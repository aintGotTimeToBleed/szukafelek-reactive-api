package pl.mzapisek.szukafelek.reactiveapi.image;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ImageRepository extends ReactiveCrudRepository<Image, Integer> {

    @Query("SELECT * FROM images WHERE uuidFileName = :name")
    Mono<Image> findByName(String name);

    @Query("SELECT * FROM images WHERE tile_id = :tileId")
    Flux<Image> findImagesByTileId(Integer tileId);

    @Query("SELECT * FROM images WHERE tile_id = :tileId AND is_main_image = 'true'")
    Mono<Image> findMainImageByTileId(Integer tileId);

}
