package pl.mzapisek.szukafelek.reactiveapi.image;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@AllArgsConstructor
@Table("images")
public class Image {

    @Id
    Integer id;
    @Column("tile_id")
    Integer tileId;
    @Column("uuid_file_name")
    String uuidFileName;
    String fileName;
    Boolean isMainImage;

}
