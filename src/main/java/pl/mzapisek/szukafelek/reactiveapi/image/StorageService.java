package pl.mzapisek.szukafelek.reactiveapi.image;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.io.File;

@Service
public class StorageService {

    private final String imagePath;

    public StorageService(@Value("${images.location}") String imagePath) {
        this.imagePath = imagePath;
    }

    public Mono<String> store(FilePart file, String fileName) {
        // todo create thumbnail
        return file.transferTo(new File(imagePath + File.separator + fileName))
                .then(Mono.just(fileName));
    }

    public Mono<Resource> load(String fileName) {
        return Mono.just(new FileSystemResource(imagePath + File.separator + fileName));
    }

}
