package pl.mzapisek.szukafelek.reactiveapi.image;

import pl.mzapisek.szukafelek.reactiveapi.exception.ResourceNotFoundException;

public class ImageNotFoundException extends ResourceNotFoundException {

    public ImageNotFoundException(String message) {
        super(message);
    }

}
