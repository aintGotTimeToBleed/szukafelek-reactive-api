package pl.mzapisek.szukafelek.reactiveapi.user;

import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Value
@Table("/users")
public class User {

    @Id
    Integer id;

    // todo authorities?

}
