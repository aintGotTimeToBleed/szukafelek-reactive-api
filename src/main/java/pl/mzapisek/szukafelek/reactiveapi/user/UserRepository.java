package pl.mzapisek.szukafelek.reactiveapi.user;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface UserRepository extends ReactiveCrudRepository<User, Integer>  {
    // todo
}
