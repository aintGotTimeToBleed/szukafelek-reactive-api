package pl.mzapisek.szukafelek.reactiveapi.tile;

import reactor.core.publisher.Flux;

import java.util.Map;

public interface SearchTileRepository {

    Flux<Tile> search(Map<String, String> requestParams);

}
