package pl.mzapisek.szukafelek.reactiveapi.tile;

import pl.mzapisek.szukafelek.reactiveapi.exception.ResourceNotFoundException;

public class TileNotFoundException extends ResourceNotFoundException {

    public TileNotFoundException(String message) {
        super(message);
    }

}
