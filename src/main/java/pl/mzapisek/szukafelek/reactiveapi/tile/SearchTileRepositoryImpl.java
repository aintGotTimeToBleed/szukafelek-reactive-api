package pl.mzapisek.szukafelek.reactiveapi.tile;

import lombok.RequiredArgsConstructor;
import org.springframework.data.r2dbc.core.DatabaseClient;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.Map;

@Repository
@RequiredArgsConstructor
public class SearchTileRepositoryImpl implements SearchTileRepository {

    private static final String TILE_TABLE = "tiles";

    private final DatabaseClient client;

    public Flux<Tile> search(Map<String, String> requestParams) {
        Criteria criteria = createCriteria(requestParams);

        return client
                .select()
                .from(TILE_TABLE)
                .matching(criteria)
                .as(Tile.class)
                .fetch()
                .all();
    }

    private Criteria createCriteria(Map<String, String> requestParams) {
        // todo add validations, currently you can pass everything, when you pass something that isn't a correct column
        // in tiles table 500 will be thrown
        Criteria criteria = Criteria.empty();
        for (Map.Entry<String, String> entry : requestParams.entrySet()) {
            if (criteria.isEmpty()) {
                criteria = Criteria.where(entry.getKey()).is(entry.getValue());
            } else {
                criteria = criteria.and(entry.getKey()).is(entry.getValue());
            }
        }

        return criteria;
    }


}
