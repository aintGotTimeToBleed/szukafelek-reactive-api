package pl.mzapisek.szukafelek.reactiveapi.tile;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@Table("tile_size")
public class TileSize {

  @Id
  Integer id;
  @NotNull
  String name;
  String description;

}
