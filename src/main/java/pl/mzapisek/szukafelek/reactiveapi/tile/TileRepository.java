package pl.mzapisek.szukafelek.reactiveapi.tile;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

interface TileRepository extends ReactiveCrudRepository<Tile, Integer> {
}
