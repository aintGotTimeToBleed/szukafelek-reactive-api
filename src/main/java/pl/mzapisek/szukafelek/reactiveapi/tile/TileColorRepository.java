package pl.mzapisek.szukafelek.reactiveapi.tile;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

interface TileColorRepository extends ReactiveCrudRepository<TileColor, Integer> {
}
