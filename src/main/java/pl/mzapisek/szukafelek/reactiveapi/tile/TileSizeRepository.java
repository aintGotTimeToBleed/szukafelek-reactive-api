package pl.mzapisek.szukafelek.reactiveapi.tile;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

interface TileSizeRepository extends ReactiveCrudRepository<TileSize, Integer> {
}
