package pl.mzapisek.szukafelek.reactiveapi.tile;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.http.codec.multipart.FormFieldPart;
import org.springframework.http.codec.multipart.Part;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;
import pl.mzapisek.szukafelek.reactiveapi.image.Image;
import pl.mzapisek.szukafelek.reactiveapi.image.ImageNotFoundException;
import pl.mzapisek.szukafelek.reactiveapi.image.ImageService;
import pl.mzapisek.szukafelek.reactiveapi.image.StorageService;
import pl.mzapisek.szukafelek.reactiveapi.offer.Offer;
import pl.mzapisek.szukafelek.reactiveapi.offer.OfferNotFoundException;
import pl.mzapisek.szukafelek.reactiveapi.offer.OfferService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.PipedOutputStream;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/tiles")
public class TileController {

    private String hostname;

    private final TileService tileService;
    private final ImageService imageService;
    private final StorageService storageService;
    private final OfferService offerService;

    public TileController(TileService tileService,
                          ImageService imageService,
                          StorageService storageService,
                          OfferService offerService,
                          @Value("${hostname}") String hostname) {
        this.tileService = tileService;
        this.imageService = imageService;
        this.storageService = storageService;
        this.offerService = offerService;
        this.hostname = hostname;
    }

    // todo, probably could be changed to QueryDSL for R2DBC - https://github.com/querydsl/querydsl/pull/2610
    @GetMapping
    public Flux<EntityModel<Tile>> getTiles(@RequestParam Map<String, String> params) {
        if (!params.isEmpty()) {
            return tileService.searchTiles(params).map(this::createTileEntityModel);
        }
        return tileService.getTiles().map(this::createTileEntityModel);
    }

    @GetMapping("{id}")
    public Mono<EntityModel<Tile>> getTile(@PathVariable Integer id) {
        return tileService.getTile(id)
                .switchIfEmpty(Mono.error(new TileNotFoundException("Tile not found")))
                .map(this::createTileEntityModel);
    }

    @PostMapping
    public Mono<EntityModel<Tile>> createTile(@RequestBody Tile tile) {
        return tileService.createTile(tile)
                .map(this::createTileEntityModel);
    }

    @PatchMapping
    public Mono<EntityModel<Tile>> updateTile(@RequestBody Tile tile) {
        return tileService.updateTile(tile)
                .map(this::createTileEntityModel);
    }

    @DeleteMapping("{id}")
    public Mono<Void> deleteTile(@PathVariable Integer id) {
        return tileService.deleteTile(id);
    }

    @PostMapping(value = "{tileId}/images")
    public Mono<EntityModel<Image>> upload(@PathVariable Integer tileId, ServerWebExchange exchange) {
        // todo move logic from controller to service
        return exchange.getMultipartData()
                .flatMap(parts -> {
                    // todo refactor this whole section
                    // todo add validation, currently if there no field set for setAsMainImage it will throw 500
                    Map<String, Part> stringPartMap = parts.toSingleValueMap();
                    String fileName = UUID.randomUUID().toString();
                    FormFieldPart name = (FormFieldPart) stringPartMap.get("name");
                    FormFieldPart setAsMainImage = (FormFieldPart) stringPartMap.get("setAsMainImage");
                    Boolean isMainImage = Boolean.valueOf(setAsMainImage.value());

//                    FilePart file = (FilePart) stringPartMap.get("file");
//                    Flux<DataBuffer> content = file.content();
//                    PipedOutputStream pipedOutputStream = new PipedOutputStream();
//                    DataBufferUtils.write(content, pipedOutputStream).subscribe(DataBufferUtils.releaseConsumer());

                    return tileService
                            .getTile(tileId)
                            .switchIfEmpty(Mono.error(new TileNotFoundException("tile not found with id: " + tileId)))
                            .flatMap(tile -> storageService.store((FilePart) stringPartMap.get("file"), fileName))
                            .flatMap(storedFileName -> imageService.saveImage(new Image(null, tileId, fileName, name.value(), isMainImage)))
                            // todo can I do something with that?
                            .flatMap(image -> {
                                // nie powinno byc zagniezdzonych flatmap
                                if (image.getIsMainImage()) {
                                    return tileService.getTile(tileId)
                                            .flatMap(tile -> {
                                                tile.setMainImageUuid(image.getUuidFileName());
                                                tile.setMainImageId(image.getId());
                                                return tileService.updateTile(tile)
                                                        .flatMap(updatedTile -> Mono.just(image));
                                            });
                                }
                                return Mono.just(image);
                            })
                            .flatMap(image -> Mono.just(createImageEntityModel(image)));
                });
    }

    @GetMapping("{id}/images")
    public Flux<EntityModel<Image>> getImagesForTile(@PathVariable Integer id,
                                                     @RequestParam(required = false, value = "isMain", defaultValue = "false") Boolean isMain) {
        return tileService.getTile(id)
                .flatMapMany(Flux::just)
                .switchIfEmpty(Flux.error(new TileNotFoundException("Tile not found of id: " + id)))
                .flatMap(tile -> isMain ? imageService.findMainImageByTileId(tile.id) : imageService.findImagesByTileId(tile.id))
                .switchIfEmpty(Flux.error(new ImageNotFoundException("Images not found for tile of id: " + id)))
                .map(this::createImageEntityModel);
    }

    @GetMapping("{id}/offers")
    public Flux<EntityModel<Offer>> getOffersForTile(@PathVariable Integer id) {
        return offerService.getOffersByTileId(id)
                .switchIfEmpty(Flux.error(new OfferNotFoundException("Offers not found for tile id: " + id)))
                .map(this::createOfferForTileEntityModel);
    }


    @GetMapping("/colors")
    public Flux<TileColor> getColors() {
        return tileService.getTileColors();
    }

    @GetMapping("/sizes")
    public Flux<TileSize> getSizes() {
        return tileService.getTileSizes();
    }

    // todo tiles/1/orders

    // todo move below methods to TileEntryModelService
    private EntityModel<Tile> createTileEntityModel(Tile tile) {
        return EntityModel.of(tile,
                Link.of(hostname + "/tiles/" + tile.getId()).withSelfRel(),
                Link.of(hostname + "/tiles/" + tile.getId() + "/images").withRel("images"),
                Link.of(hostname + "/images/" + tile.getMainImageId() + "/download").withRel("download main image"));
    }

    private EntityModel<Image> createImageForTileEntityModel(Image image) {
        return EntityModel.of(image,
                Link.of(hostname + "/images/" + image.getId()).withSelfRel(),
                Link.of(hostname + "/images/" + image.getId() + "/download").withRel("download image"),
                Link.of(hostname + "/tiles/" + image.getId() + "/images").withRel("images"));
    }

    private EntityModel<Image> createImageEntityModel(Image image) {
        return EntityModel.of(image,
                Link.of(hostname + "/images/" + image.getId()).withSelfRel(),
                Link.of(hostname + "/images/" + image.getId() + "/download").withRel("download image"),
                Link.of(hostname + "/tiles/" + image.getId()).withRel("tiles"));
    }

    private EntityModel<Offer> createOfferForTileEntityModel(Offer offer) {
        return EntityModel.of(offer,
                Link.of(hostname + "/offers/" + offer.getId()).withSelfRel(),
                Link.of(hostname + "/offers/" + offer.getId() + "/tiles").withRel("tiles"));
    }

}
