package pl.mzapisek.szukafelek.reactiveapi.tile;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import javax.validation.constraints.NotNull;

// todo make it immutable
@Data
@AllArgsConstructor
@Table("tiles")
public class Tile {

    @Id
    Integer id;
    String uuid;
    @NotNull
    String name;
    @NotNull
    String size;
    String description;
    Boolean popular;
    Integer mainImageId;
    String mainImageUuid;
    // todo add validation
    String color;
    String producerName;
    Double producerPrice;
    String producerPriceUnit;

}
