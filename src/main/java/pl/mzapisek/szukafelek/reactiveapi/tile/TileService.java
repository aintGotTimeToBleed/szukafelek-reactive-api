package pl.mzapisek.szukafelek.reactiveapi.tile;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TileService {

    private final TileRepository tileRepository;
    private final SearchTileRepository searchTileRepository;
    private final TileColorRepository tileColorRepository;
    private final TileSizeRepository tileSizeRepository;

    public Flux<Tile> getTiles() {
        return tileRepository.findAll();
    }

    public Mono<Tile> getTile(Integer id) {
        return tileRepository.findById(id);
    }

    public Mono<Tile> createTile(Tile tile) {
        tile.setUuid(UUID.randomUUID().toString());

        return tileRepository.save(tile);
    }

    public Mono<Tile> updateTile(Tile tile) {
        return tileRepository.save(tile);
    }

    public Mono<Void> deleteTile(Integer id) {
        return tileRepository.deleteById(id);
    }

    public Flux<Tile> searchTiles(Map<String, String> params) {
        return searchTileRepository.search(params);
    }

    // todo separateservice?
    public Flux<TileColor> getTileColors() {
        return tileColorRepository.findAll();
    }

    public Flux<TileSize> getTileSizes() {
        return tileSizeRepository.findAll();
    }

}
