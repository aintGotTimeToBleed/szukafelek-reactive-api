package pl.mzapisek.szukafelek.reactiveapi.offer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.*;
import pl.mzapisek.szukafelek.reactiveapi.tile.Tile;
import pl.mzapisek.szukafelek.reactiveapi.tile.TileNotFoundException;
import pl.mzapisek.szukafelek.reactiveapi.tile.TileService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("/offers")
public class OfferController {

    private String hostname;

    private final OfferService offerService;
    private final TileService tileService;

    public OfferController(OfferService offerService,
                           TileService tileService,
                           @Value("${hostname}") String hostname) {
        this.offerService = offerService;
        this.tileService = tileService;
        this.hostname = hostname;
    }

    @GetMapping
    public Flux<EntityModel<Offer>> getOffers() {
        return offerService.getOffers()
                .map(this::createOfferEntityModel);
    }

    @GetMapping("{id}")
    public Mono<EntityModel<Offer>> getOffer(@PathVariable Integer id) {
        return offerService.getOffer(id)
                .switchIfEmpty(Mono.error(new OfferNotFoundException("Offer not found with id: " + id)))
                .map(this::createOfferEntityModel);
    }

    @GetMapping("{id}/tiles")
    public Mono<EntityModel<Tile>> getTileForOffer(@PathVariable Integer id) {
        return offerService.getOffer(id)
                .switchIfEmpty(Mono.error(new OfferNotFoundException("Offer not found with id: " + id)))
                .flatMap(offer -> tileService.getTile(offer.tileId)
                        .switchIfEmpty(Mono.error(new TileNotFoundException("Tile not with id: " + offer.getTileId())))
                        .map(this::createTileEntityModel));
    }

    @PostMapping
    public Mono<EntityModel<Offer>> createOffer(@RequestBody @Valid Offer offer) {
        return tileService.getTile(offer.id)
                .switchIfEmpty(Mono.error(new TileNotFoundException("Tile not found for id: " + offer.id)))
                .flatMap(tile -> offerService.createOffer(offer))
                .map(this::createOfferEntityModel);
    }

    @DeleteMapping("{id}")
    public Mono<Void> deleteOffer(@PathVariable Integer id) {
        return offerService.deleteOffer(id);
    }

    private EntityModel<Offer> createOfferEntityModel(Offer offer) {
        return EntityModel.of(offer,
                Link.of(hostname + "/offers/" + offer.getId()).withSelfRel(),
                Link.of(hostname + "/offers/" + offer.getId() + "/tiles").withRel("tiles"));
    }

    private EntityModel<Tile> createTileEntityModel(Tile tile) {
        return EntityModel.of(tile,
                Link.of(hostname + "/tiles/" + tile.getId()).withSelfRel(),
                Link.of(hostname + "/tiles/" + tile.getId() + "/offers").withRel("offers"));
    }

}
