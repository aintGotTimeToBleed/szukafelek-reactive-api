package pl.mzapisek.szukafelek.reactiveapi.offer;

import pl.mzapisek.szukafelek.reactiveapi.exception.ResourceNotFoundException;

public class OfferNotFoundException extends ResourceNotFoundException {

    public OfferNotFoundException(String message) {
        super(message);
    }

}
