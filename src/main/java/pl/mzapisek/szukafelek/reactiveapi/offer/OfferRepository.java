package pl.mzapisek.szukafelek.reactiveapi.offer;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

interface OfferRepository extends ReactiveCrudRepository<Offer, Integer> {

    @Query("SELECT * FROM offer WHERE tile_id = :tileId")
    Flux<Offer> findByTileId(Integer tileId);

}
