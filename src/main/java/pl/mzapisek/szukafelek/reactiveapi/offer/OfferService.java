package pl.mzapisek.szukafelek.reactiveapi.offer;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class OfferService {

    private final OfferRepository offerRepository;

    public OfferService(OfferRepository offerRepository) {
        this.offerRepository = offerRepository;
    }

    public Flux<Offer> getOffers() {
        return offerRepository.findAll();
    }

    public Mono<Offer> getOffer(Integer id) {
        return offerRepository.findById(id);
    }

    public Mono<Offer> createOffer(Offer offer) {
        // todo verify whether same seller try to create another offer to same tile
            // new offer should override previous one
        return offerRepository.save(offer);
    }

    public Mono<Void> deleteOffer(Integer id) {
        return offerRepository.deleteById(id);
    }

    public Flux<Offer> getOffersByTileId(Integer id) {
        return offerRepository.findByTileId(id);
    }

}
