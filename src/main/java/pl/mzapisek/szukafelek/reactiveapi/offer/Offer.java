package pl.mzapisek.szukafelek.reactiveapi.offer;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@Table("offers")
public class Offer {

    @Id
    Integer id;
    @Column(value = "tile_id")
    Integer tileId;
    BigDecimal price;
    String seller;

}
