package pl.mzapisek.szukafelek.reactiveapi.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

// todo for liquibase only, why autoconfiguration doesnt work?
@Configuration
public class DbDatasourceConfiguration {

  @Value("${spring.datasource.driver-class-name}")
  String driverClassName;
  @Value("${spring.datasource.url}")
  String url;
  @Value("${spring.datasource.username}")
  String userName;
  @Value("${spring.datasource.password}")
  String password;

  @Bean
  public DataSource getDataSource() {
    DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
    dataSourceBuilder.driverClassName(driverClassName);
    dataSourceBuilder.url(url);
    dataSourceBuilder.username(userName);
    dataSourceBuilder.password(password);

    return dataSourceBuilder.build();
  }

}
