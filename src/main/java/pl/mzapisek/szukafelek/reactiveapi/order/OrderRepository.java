package pl.mzapisek.szukafelek.reactiveapi.order;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

interface OrderRepository extends ReactiveCrudRepository<Order, Integer> {
}
