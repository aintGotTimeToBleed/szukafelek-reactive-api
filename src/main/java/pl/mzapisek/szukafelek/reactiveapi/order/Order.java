package pl.mzapisek.szukafelek.reactiveapi.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@Table("orders")
public class Order {

    @Id
    Integer id;
    Integer tileId;
    String uuid;
    Integer quantity;
    BigDecimal totalCost;

}
