DROP TABLE IF EXISTS tiles;

CREATE TABLE tiles
(
  id                  int primary key auto_increment,
  uuid                varchar(36),
  name                varchar(255),
  size                varchar(255),
  description         varchar(255),
  popular             boolean,
  main_image_id       int, -- todo fk -> images.id ??
  main_image_uuid     varchar(255),
  color               varchar(255),
  producer_name       varchar(255),
  producer_price      decimal(6, 2),
  producer_price_unit varchar(255)
);

CREATE TABLE offers
(
  id      int primary key auto_increment,
  tile_id int not null,
  price   decimal(6, 2),
  seller  varchar(255),
  FOREIGN KEY (tile_id) REFERENCES tiles (id)
);

-- TODO ADD ON DELETE CONSTRAINTS

CREATE TABLE images
(
  id             int primary key auto_increment,
  tile_id        int not null,
  uuid_file_name varchar(36) UNIQUE,
  file_name      varchar(255),
  is_main_image  boolean,
  FOREIGN KEY (tile_id) REFERENCES tiles (id)
);

CREATE TABLE orders
(
  id      int primary key auto_increment,
  tile_id int not null,
  uuid    varchar(36),
  -- name    varchar(255),
  FOREIGN KEY (tile_id) REFERENCES tiles (id)
);

CREATE TABLE users
(
  id        INT PRIMARY KEY AUTO_INCREMENT,
  type      ENUM ('buyer', 'seller') NOT NULL,
  name      VARCHAR(255),
  last_name VARCHAR(255),
  username  VARCHAR(255) UNIQUE, -- email
  email     VARCHAR(255),
  password  VARCHAR(100)
);

CREATE TABLE authorities
(
  username  VARCHAR(50) NOT NULL,
  authority VARCHAR(50) NOT NULL,
  FOREIGN KEY (username) REFERENCES users (username)
);

CREATE TABLE tile_color
(
  id              int primary key auto_increment,
  name            varchar(255),
  description     varchar(255),
  hex_color_codes varchar(255),
  is_multi_color  boolean
);

CREATE TABLE tile_size
(
  id          int primary key auto_increment,
  name        varchar(255),
  description varchar(255)
);