## Szukafelek Reactive API
Simple tile shop. 

Just another practice project. Work in progress. 

### Technologies
- jdk 11
- spring boot (webflux, hateos, security, actuator, swagger)
- r2dbc, mysql, liquibase

### Starting locally
```docker-compose up -d```

### Open API / Swagger
* localhost:8080/swagger-ui.html

## todo
* add testcontainers
* ssl
* user jdbc auth (username, password in headers?)
* validators

## examples
* https://www.pakamera.pl/
* https://www.burton.com/us/en/home
* https://www.reebok.pl

#### Useful reads
https://www.youtube.com/watch?v=F6f3Rbl965w -> webflux + angular
https://www.youtube.com/watch?v=xQEJFUPeQ_8 -> r2dbc
https://spring.io/projects/spring-data-r2dbc

https://dzone.com/articles/applying-hateoas-to-a-rest-api-with-spring-boot
https://www.callicoder.com/spring-boot-file-upload-download-rest-api-example/

Hateoas example 
https://github.com/toedter/movies-demo/blob/master/backend/src/main/java/com/toedter/movies/movie/MovieController.java

JDBC SECURITY
https://www.baeldung.com/spring-security-jdbc-authentication

https://stackoverflow.com/questions/49259156/spring-webflux-serve-files-from-controller
https://spring.io/guides/gs/uploading-files/
https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Disposition
https://stackoverflow.com/questions/52491405/how-to-combine-flux-and-responseentity-in-spring-webflux-controllers


#### populate db
```
INSERT INTO tiles (uuid, name, size, description, popular, main_image_id , color , producer_name , producer_price , producer_price_unit)
VALUES
       ('b7f9352b-9c9c-4c45-9a09-96c8b59c8530', 'test', '30x30', '', true, null, 'green', 'zapis', 33.33, 'szt'),
       ('f881d6a6-26a8-40a4-b8fb-749c49ce811f', 'test2', '30x30', '', false, null, 'green', 'zapis', 50, 'szt'),
       ('36c5b77e-8c1e-4a29-8951-6910ed1a28bc', 'test3', '30x30', '', true, null, 'yellow', 'zapis', 33.33, 'szt');
   
INSERT INTO tiles (uuid, name, size, description, popular, main_image_id , color , producer_name , producer_price , producer_price_unit)
VALUES
       ('b7f9352b-9c9c-4c45-9a09-96c8b59c8531', 'test4', '30x30', '', true, null, 'green', 'zapis', 33.33, 'szt'),
       ('f881d6a6-26a8-40a4-b8fb-749c49ce8112', 'test5', '30x30', '', false, null, 'green', 'zapis', 50, 'szt'),
       ('36c5b77e-8c1e-4a29-8951-6910ed1a28b3', 'test6', '30x30', '', true, null, 'yellow', 'zapis', 33.33, 'szt'),
       ('f881d6a6-26a8-40a4-b8fb-749c49ce8114', 'test7', '30x30', '', false, null, 'green', 'zapis', 50, 'szt'),
       ('36c5b77e-8c1e-4a29-8951-6910ed1a28b5', 'test8', '30x30', '', true, null, 'yellow', 'zapis', 33.33, 'szt');   
       
INSERT INTO szukafelek.tile_color (name,description,hex_color_codes,is_multi_color) VALUES 
('red','czerwony','#e63f2e',0),
('orange','pomarańczowy','#e6962e',0),
('yellow','żółty','#f5ce39',0),
('black', 'czarny', '#333', false),
('brown', 'brązowy', '#6a5631', false),
('green', 'zielony', '#2f7a58', false),
('blue', 'niebieski', '#499dba', false),
('pink', 'różowy', '#eabbbb', false),
('grey', 'szary', 'grey', false),
('silver', 'srebrny', 'silver', false),
('purple', 'fioletowy', 'purple', false),
('lightblue', 'jasny niebieski', 'lightblue', false),
('black-white', 'czarno biały', 'black, white',true),
('white', 'biały', 'white', false);

INSERT INTO szukafelek.tile_size (name, description) VALUES
('15x15', '15x15'),
('30x30', '30x30');

```